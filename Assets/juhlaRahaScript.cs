﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;


public class juhlaRahaScript : MonoBehaviour {

    public InputField urli;
    public Texture2D loadedTexture;
    public Texture2D targetTexture;
    public Renderer kolike;
    public GameObject menuitems;
    public RenderTexture renderTex;
    public RenderTexture polaroidTex;
    public Camera polaroidCam;
    Camera currentCam;
    Texture2D tex2D;
    bool m_bTakingScreenshot = false;

    void Awake()
    {
        currentCam = Camera.main;
    }

	// Use this for initialization
	void Start ()
    {
        #if !UNITY_EDITOR && UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = false;
        #endif
    }

    // n.SetPixel(x,y,new Color( Mathf.Clamp01(sCurve(xDelta, distortion)) , Mathf.Clamp01(sCurve(yDelta, distortion)),1f,1.0f));

    public static float sCurve(float x, float distortion)
    {
        return 1f / (1f + Mathf.Exp(-Mathf.Lerp(5f, 15f, distortion) * (x - 0.5f)));
    }

    public void fixLoadedTexture()
    {
        for (int i = 0; i < loadedTexture.width; i++)
        {
            for (int j = 0; j < loadedTexture.height; i++)
            {
                loadedTexture.SetPixel(i, j, new Color(Mathf.Clamp01(sCurve(1f, 10f)), Mathf.Clamp01(sCurve(1f, 10f)), 1f, 1.0f));
            }
        }
        loadedTexture.Apply();
    }

    public void loadPic()
    {
        StartCoroutine(loadPicNow());

    }

    IEnumerator loadPicNow()
    {
        WWW www = new WWW(urli.text);
        ///UnityWebRequest www = UnityWebRequest.GetTexture(urli.text);
        yield return www;
        if (www.error != null)
        {
            Debug.Log(www.error);
        }
        else
        {
            //loadedTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            www.LoadImageIntoTexture(loadedTexture);
            //loadedTexture.LoadRawTextureData( ((DownloadHandlerTexture)www.downloadHandler).texture.GetRawTextureData());
            //targetTexture.LoadRawTextureData(loadedTexture.GetRawTextureData());
            //targetTexture.LoadRawTextureData(loadedTexture.GetRawTextureData());
            //targetTexture.Apply();
        }
        //fixLoadedTexture();
        targetTexture = toNormalMap(loadedTexture);
        kolike.material.SetTexture("_BumpMap", targetTexture);
    }

    public void HideUI()
    {
        menuitems.SetActive(false);
    }

    public void ShowUI()
    {
        menuitems.SetActive(true);
    }

    public void TakeScreenshot()
    {
        if (m_bTakingScreenshot) return;
        m_bTakingScreenshot = true;
        HideUI();
        currentCam.targetTexture = renderTex;
        StartCoroutine(letAFrameRun());
    }

    IEnumerator letAFrameRun()
    {
        //currentCam.Render();
        yield return new WaitForEndOfFrame();
        currentCam.targetTexture = null;
        RenderTexture.active = renderTex;

        yield return new WaitForEndOfFrame();
        //renderTex.antiAliasing = 1;
        //polaroidCam.targetTexture = polaroidTex;
        polaroidCam.Render();
        yield return new WaitForEndOfFrame();
        //Debug.Break();
        tex2D.ReadPixels(new Rect(0f, 0f, polaroidTex.width, polaroidTex.height), 0, 0);
        //polaroidCam.targetTexture = null;
        tex2D.Apply();
        RenderTexture.active = null;
        StartCoroutine(OpenDataURL());
        ShowUI();
    }

    IEnumerator OpenDataURL()
    {
        yield return null;
        byte[] pngData = tex2D.EncodeToPNG();
        yield return null;
        string base64Data = Convert.ToBase64String(pngData);
        yield return null;
        m_bTakingScreenshot = false;
        Application.ExternalEval("window.open(\"data:image/png;base64," + base64Data + "\")");
    }

    public Texture2D toNormalMap(Texture2D t)
    {
        
        Texture2D n = new Texture2D(t.width, t.height, TextureFormat.RGBA32, false);
        Color oldColor = new Color();
        Color newColor = new Color();

        for (int x = 0; x < t.width; x++)
        {
            for (int y = 0; y < t.height; y++)
            {

                oldColor = t.GetPixel(x, y);
                newColor.r = 0.3f;
                newColor.b = 0.3f;
                newColor.g = 1f - (oldColor.g*0.3f);
                newColor.a = oldColor.r;
                n.SetPixel(x, y, newColor);
            }
        }

        n.Apply();

        return n;
    }
 
 
	// Update is called once per frame
	void Update () {
		
	}
}
